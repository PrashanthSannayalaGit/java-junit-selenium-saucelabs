package com.yourcompany.Tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class FollowLinkTest extends TestBase {

    public FollowLinkTest(String os,
                          String version, String browser, String deviceName, String deviceOrientation) {
            super(os, version, browser, deviceName, deviceOrientation);
    }

    /**
     * Runs a simple test verifying link can be followed.
     * @throws InvalidElementStateException
     */
    @Test
    public void verifyLinkTest() throws InvalidElementStateException {

        Date startTime;
        Date endTime;
        long diffTime;

        startTime = new Date();
        driver.get("https://capdev408.uk.ba.com/travel/home/public/en_gb");
        endTime = new Date();

        diffTime = endTime.getTime() - startTime.getTime();

        System.out.println("The URL load on " + browser + " took: " + formatMilliseconds(diffTime));

        startTime = new Date();
        driver.getTitle();
        endTime = new Date();

        diffTime = endTime.getTime() - startTime.getTime();

        System.out.println("Getting title on " + browser + " took: " + formatMilliseconds(diffTime));

        startTime = new Date();
        //driver.findElement(By.partialLinkText("Register now")).getText();
        driver.findElement(By.xpath("//span[contains(text(),'Login/Register')]")).getText();
        endTime = new Date();

        diffTime = endTime.getTime() - startTime.getTime();

        System.out.println("Finding element on " + browser + " took: " + formatMilliseconds(diffTime));
    }

    private String formatMilliseconds(long millis) {
        String returnString;

        if (millis < 1000) {
            //we should present millis and not seconds
            returnString = millis + " milliseconds";
        } 
        else {
            returnString = TimeUnit.MILLISECONDS.toSeconds(millis) + " seconds";
        }

        return returnString;
    }
}